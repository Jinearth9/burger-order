import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://react-my-burger-8302e.firebaseio.com/'
});

export default instance